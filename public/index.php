<?php
ini_set('display_errors', 1);
define ("BASEDIR", dirname(__FILE__, 2));


require_once BASEDIR . '/app/config.php';
require_once BASEDIR . '/app/db_connect.php';
require_once BASEDIR . '/app/route.php';

Route::start();