<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Task manager</title>
    <link rel="stylesheet" href="<?php echo PUBLICURL; ?>/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo PUBLICURL; ?>/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo PUBLICURL; ?>/assets/fonts/font-awesome.min.css">
	<link rel="shortcut icon" href="<?php echo PUBLICURL; ?>/assets/img/favicon.ico" type="image/x-icon">	
</head>

<body id="page-top">
	<header class="bg-primary text-white text-center mb-4" style="height: 120px;">
		<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-secondary text-uppercase" id="mainNav">
			<div class="container"><a class="navbar-brand js-scroll-trigger" href="<?php echo  BASE; ?>">Task manager</a><button data-toggle="collapse" data-target="#navbarResponsive" class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" aria-controls="navbarResponsive"
					aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
				<div class="collapse navbar-collapse" id="navbarResponsive">
					<ul class="nav navbar-nav ml-auto">
						<li class="nav-item mx-0 mx-lg-1" role="presentation">
						<?php 
						if ($_SESSION['isadmin']==0) {
							echo '<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" 
								href="' . BASE . '/user/login">Log in</a>';
						} else {
							echo '<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" 
								href="' .BASE . '/user/logout">Log out</a>';
						} 
						?>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
    
        <div class="container mt-4" >
			<?php
				if (isset($_SESSION['error'])){
					echo '<div class="alert alert-warning" role="alert">';
					echo $_SESSION['error'];
					echo '</div>';
					unset($_SESSION['error']);
				}
				
				if (isset($_SESSION['message'])){
					echo '<div class="alert alert-info" role="alert">';
					echo $_SESSION['message'];
					echo '</div>';
					unset($_SESSION['message']);
				}
				
				if (isset($data['content'])){
					echo $data['content'];
				}
		
				if (isset($data['includs']['aftercontent'])){
					foreach ($data['includs']['aftercontent'] as $view) {
						include BASEDIR . "/app/views/". $view .".php";
					}
				}
			?>
		
		
	</div>
    <div class="copyright py-4 text-center text-white">
        <div class="container"><small>©&nbsp; <?php echo date("Y");?></small></div>
    </div>
    <div class="d-lg-none scroll-to-top position-fixed rounded"><a class="d-block js-scroll-trigger text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a></div>
    
    <script src="<?php echo PUBLICURL; ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo PUBLICURL; ?>/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
</body>

</html>