<div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Имя <a href="<?php echo BASE; ?>/task/sorting/name_asc"><i class="fa fa-sort-amount-asc"></i></a> 
									<a href="<?php echo BASE; ?>/task/sorting/name_desc"><i class="fa fa-sort-amount-desc"></i></a></th>
                            <th>Email <a href="<?php echo BASE; ?>/task/sorting/email_asc"><i class="fa fa-sort-amount-asc"></i></a>
									  <a href="<?php echo BASE; ?>/task/sorting/email_desc"><i class="fa fa-sort-amount-desc"></i></a></th>
                            <th>Task </th>
                            <th>Статус <a href="<?php echo BASE; ?>/task/sorting/status_asc"><i class="fa fa-sort-amount-asc"></i></a>
									   <a href="<?php echo BASE; ?>/task/sorting/status_desc"><i class="fa fa-sort-amount-desc"></i></a></th>
							<?php
							if ($_SESSION['isadmin']==1) {
								echo '<th>edit</th>';
							}
							?>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					foreach($data['tasks'] as $row){
						echo "<tr>
                            <td>".$row['name']."</td>
                            <td>".$row['email']."</td>
                            <td>".$row['task']."</td>
                            <td>".$status[$row['status']].$edited[$row['edited']]."</td>";
							if ($_SESSION['isadmin']==1) {
								echo '<td><a href="'. BASE .'/task/edit/'.$row['id'].'"><i class="fa fa-pencil-square-o"></i></a></td>';
							}

						echo "</tr>";
						
					}
					?>
                        
                        
                    </tbody>
                </table>
            </div>
