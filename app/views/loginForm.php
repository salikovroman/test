<div class="container">
			<div class="row">
				<h4>Авторизация</h4>
			</div>
			<div class="row">
				<form action="<?php echo BASE; ?>/user/auth" method="post">
					<div class="form-group row">
						<div class="col-lg-3">
							<label for="log">Логин</label> 
						</div>
						<div class="col-lg-9">
							<input class="form-control forminput" type="text" id="log" name="log" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label for="pass">Пароль</label> 
						</div>
						<div class="col-lg-9">
							<input class="form-control forminput" type="password" id="pass" name="pass" required>
						</div>
					</div>
					<input class="btn btn-primary" type="submit" value="Вход"></input>
				</form>
			
			</div>
		</div>