<nav>
            <ul class="pagination">
				<?php
				for ($i=1; $i<=$pages; $i++){
					echo '<li class="page-item';
					if($i==$curPage){
						echo " active";
					}
					echo '"><a class="page-link" href="'.BASE.'/task/index/page_'.$i.'">'.$i.'</a></li>';
				}
				?>
            </ul>
        </nav>