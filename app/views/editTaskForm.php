<div class="container">
			<div class="row">
				<h4>Редактировать задачу №<?php echo $item['id']; ?></h4>
			</div>
			<div class="row">
				<form action="<?php echo BASE; ?>/task/update/<?php echo $item['id']; ?>" method="post">
					<div class="form-group row">
						<div class="col-lg-3">
							<label for="name">Имя</label> 
						</div>
						<div class="col-lg-9">
							<input class="form-control forminput" disabled type="text" id="name" name="name" value="<?php echo $item['name']; ?>" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label for="email">E-mail</label> 
						</div>
						<div class="col-lg-9">
							<input class="form-control forminput" disabled type="email" id="email" name="email" value="<?php echo $item['email']; ?>" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label for="task">Задача</label>
						</div>
						<div class="col-lg-9">
							<textarea class="form-control forminput" id="task" name="task" required><?php echo $item['task']; ?> </textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label for="ready">Выполнено</label>
						</div>
						<div class="col-lg-9">
							<input class="" type="checkbox" id="ready" name="ready" 
							<?php 
							if ($item['status']==1) {
								echo " checked";
							}
							?>
							>
						</div>
					</div>
					<input class="btn btn-primary" type="submit" value="Сохранить"></input>
				</form>
			
			</div>
		</div>