<div class="container">
			<div class="row">
				<h4>Добавить задачу</h4>
			</div>
			<div class="row">
				<form action="<?php echo BASE; ?>/task/addnew" method="post">
					<div class="form-group row">
						<div class="col-lg-3">
							<label for="name">Имя</label> 
						</div>
						<div class="col-lg-9">
							<input class="form-control forminput" type="text" id="name" name="name" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label for="email">E-mail</label> 
						</div>
						<div class="col-lg-9">
							<input class="form-control forminput" type="email" id="email" name="email" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label for="task">Задача</label>
						</div>
						<div class="col-lg-9">
							<textarea class="form-control forminput" id="task" name="task" required></textarea>
						</div>
					</div>
					<input class="btn btn-primary" type="submit"></input>
				</form>
			
			</div>
		</div>