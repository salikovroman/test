<?php
session_start();
define ("BASE", "/test");
define ("PUBLICURL", "/test/public");
$database_name = "task.db";
define ("PERPAGE", 3);
define ("ASMINLOG", "admin");
define ("ASMINPAS", "123");
if(!isset($_SESSION['page'])) {
	$_SESSION['page'] = 1;
}
if(!isset($_SESSION['sort'])) {
	$_SESSION['sort'] = 'DESC';
}
if(!isset($_SESSION['sortby'])) {
	$_SESSION['sortby'] = 'id';
}
if(!isset($_SESSION['isadmin'])) {
	$_SESSION['isadmin'] = 0;
}
