<?php

class Route {
	static function start() {
		$controller_name = "task";
		$action_name = "index";
		
		$routes = explode('/', $_SERVER['REQUEST_URI']);
		
		$i=1;
		while(true){
			if(!empty($routes[$i])){
				if(is_dir($_SERVER['DOCUMENT_ROOT'] . $routes[$i])){
					$i++;
				}else{
					break;
				}
			}else{
				break;
			}
		}

		if (!empty($routes[$i])){
			$controller_name = $routes[$i];
		}

		if (!empty($routes[$i+1])){
			$action_name = $routes[$i+1];
		}
		
		if (!empty($routes[$i+2])){
			$params = $routes[$i+2];
			if(substr($params, 0, 5) == "page_"){
				$_SESSION['page'] = substr($params, 5);
				unset($params);
			}
		}
		
		$model_name = 'Model_' . $controller_name;
		$controller_name = 'Controller_' . $controller_name;
		
		$model_path = BASEDIR . "/app/models/" . $model_name . ".php";
		if(file_exists($model_path)) {
			include $model_path;
		}
		
		$controller_path = BASEDIR . "/app/controllers/" . $controller_name . ".php";
		if(file_exists($controller_path)){
			include $controller_path;
		}
		else {
			Route::ErrorPage404();
		}
		
		$controller = new $controller_name;
		$action = $action_name;
		
		if(method_exists($controller, $action))
		{
			if(isset($params)){
				$controller->$action($params);
			}else{
				$controller->$action();
			}
		}
		else
		{
			Route::ErrorPage404();
		}		
	}
	
	
	static function ErrorPage404()	{
        header('Location:'. BASE .'/404');
    }
}