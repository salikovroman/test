<?php
$db = new PDO("sqlite:" . BASEDIR . "/app/" . $database_name);
$query = "CREATE TABLE IF NOT EXISTS tasks (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
	name varchar(255), 
	email varchar(255), 
	task TEXT,
	status integer DEFAULT 0,
	edited integer DEFAULT 0 
	)";
$db->exec($query);

?>