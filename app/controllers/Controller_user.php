<?php
class Controller_user {
	public $model;
	
	function __construct()
	{
		$this->model = new Model_user();
	}
	
	public function logout(){
		$_SESSION['isadmin'] = 0;
		$_SESSION['message'] = "Вы вышли из системы";
		header('Location: ' . $_SERVER['HTTP_REFERER']); 
	}
	
	public function login(){
		$data['includs']['aftercontent'][] = 'loginForm';
		include BASEDIR . "/app/views/commonView.php";
	}
	
	public function auth(){
		if(isset($_POST))
		if($this->model->checkAdmin($_POST['log'], $_POST['pass'])) {
			$_SESSION['isadmin'] = 1;
			$_SESSION['message'] = "Авторизация прошла успешно";
			header('Location: ' . BASE . "/"); 
		}else {
			$_SESSION['error'] = "Ошибка авторизации";
			header('Location: ' . $_SERVER['HTTP_REFERER']); 
		}
	}
}