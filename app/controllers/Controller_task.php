<?php

class Controller_task {
	public $model;
		
	function __construct()
	{
		$this->model = new Model_task();
		
	}
	
	public function index(){
		$total = $this->model->numRows("tasks");
		$modelData = $this->model->get_data($_SESSION['page'], $_SESSION['sort'], $_SESSION['sortby']);
		$data['tasks'] = $modelData;
		$status = array("новый", "выполнено");
		$edited = array("", '<br><span class="small">обновлено администратором</span>');
		$data['includs']['aftercontent'][] = 'taskTable';
		if($total > PERPAGE) {
			$curPage = $_SESSION['page'];
			$pages = ceil($total/PERPAGE);
			$data['includs']['aftercontent'][] = 'pagination';
		}
		$data['includs']['aftercontent'][] = 'addNewTaskForm';
		include BASEDIR . "/app/views/commonView.php";
	}
	
	public function addnew(){
		if(isset($_POST)){
			$data['name'] = strip_tags($_POST['name']);
			$data['email'] = strip_tags($_POST['email']);
			$data['task'] = strip_tags($_POST['task']);
			
			$result = $this->model->save_data($data);
			if($result){
				$_SESSION['message'] = "Данные успешно добавлены";
			}else{
				$_SESSION['error'] = "Ошибка добавления в БД!";
			}
			header('Location: ' . $_SERVER['HTTP_REFERER']); 
			
		}
	}
	
	public function sorting($params) {
		$sortarr = explode("_", $params);
		$_SESSION['sortby'] = $sortarr[0];
		$_SESSION['sort'] = $sortarr[1];
		header('Location: ' . $_SERVER['HTTP_REFERER']); 
	}
	
	public function edit($id){
		$item = $this->model->get_item($id);
		if($_SESSION['isadmin']==1){
			if($item) {
				$data['includs']['aftercontent'][] = 'editTaskForm';
				include BASEDIR . "/app/views/commonView.php";
			}else {
				$_SESSION['error'] = "Запись не найдена";
				header('Location: ' . BASE . "/"); 
			}
		}else{
			$_SESSION['error'] = "Ошибка. Доступно только администратору";
			include BASEDIR . "/app/views/commonView.php";
		}
	}
	
	public function update($id) {
		if($_SESSION['isadmin']==1){
			if(isset($_POST)){
				$toUpdate = false;
				$itemOld = $this->model->get_item($id);
				$data['id']=(int)$id;
				$data['edited']=(int)$itemOld['edited'];
				$data['status']=(int)$itemOld['status'];
				$data['task'] = strip_tags($_POST['task']);
				
				if($data['task'] != $itemOld['task']) {
					$toUpdate = true;
					$data['edited']=1;
				}
				
				if(isset($_POST['ready'])){
					$toUpdate = true;
					$data['status']=1;
				}
				
				if($toUpdate){
					$upd = $this->model->update_item($data);
					if($upd) {
						$_SESSION['message'] = "Успешно обновлено";
						header('Location: ' . BASE . "/");
					}else {
						$_SESSION['error'] = "Ошибка. Запись не обновлена";
						header('Location: ' . BASE . "/"); 
					}
				}
			}
		}else{
			$_SESSION['error'] = "Ошибка. Сессия завершена";
			header('Location: ' . BASE . "/"); 
		}
	}
}