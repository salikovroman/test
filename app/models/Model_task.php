<?php

class Model_task {
	public function get_data($page, $sort, $sortby) {
		global $db;
		
		$data=array();
		$start = ($page - 1) * PERPAGE;
		$query = "SELECT * from `tasks` order by $sortby $sort LIMIT ".$start.", " . PERPAGE;
		$stmt = $db->query($query);
		while ($row = $stmt->fetch())
		{
			 $data[] = array(
				'id'=> $row['id'],
				'name'=> $row['name'],
				'email'=> $row['email'],
				'task'=> $row['task'],
				'status'=> $row['status'],
				'edited'=> $row['edited']
			 );
		}
		return $data;
	}
	
	public function save_data($data){
	global $db;
		$stmt = $db->prepare("INSERT into tasks (name, email, task) values (?, ?, ?)");
		return $stmt->execute(array($data['name'], $data['email'], $data['task']));
	}
	
	public function numRows($tbl) {
	global $db;
		$stat = $db->query("SELECT COUNT(*) FROM $tbl")->fetchColumn();
		return $stat;

	}
	
	public function get_item($id) {
	global $db;
		$stmt = $db->prepare("SELECT * from `tasks` where id = ? limit 1");
		$stmt->execute(array($id));
		$item=$stmt->fetchAll();
		if($item) {
			return $item[0];
		}else{
			return false;
		}
		
	}
	
	public function update_item($data) {
	global $db;
		$stmt = $db->prepare("UPDATE tasks set task = :task, edited = :edt, status = :st where id = :id");
		$stmt->bindParam(':task',$data['task']);
		$stmt->bindParam(':edt',$data['edited']);
		$stmt->bindParam(':st',$data['status']);
		$stmt->bindParam(':id',$data['id']);
		$stmt->execute();
		return $stmt;
	}
}


